const watch = require('node-watch');
const fs = require('fs-extra');
const path = require('path');

const cfg = fs.readJsonSync("./config.json", { throws: true })
checkConfig(cfg)
let webclientDir = cfg.assetmaxWebClientDir
let webDir = path.join(cfg.assetmaxDir, "modules/app-assetmax/web")
watch(webclientDir, { recursive: true, filter: _filter }, function (evt, name) {
  let destination = name.replace(webclientDir, webDir)
  console.log(destination, name)
  if (evt == "remove") {
    if (cfg.propagateDelete) {
      fs.removeSync(destination)
      logEvent(evt, destination)
    } else {
      logEvent("ignoreing deletion of ", name)
    }

  } else {
    fs.copySync(name, destination)
    logEvent(evt, destination)
  }

});

console.log("Tracking directory " + webclientDir)
console.log("propagating changes to " + webDir)
if (!cfg.propagateDelete) console.log("Ignoreing deletions");

function logEvent(msg, file) {
  let time = new Date()
  console.log(`${time.getHours()}:${time.getMinutes()} ${msg} ${file}`)
}
function _filter(file) {
  return file.endsWith(".js") || file.endsWith(".css") || file.endsWith(".html") || file.endsWith(".json") || file.endsWith(".txt")
}
function checkConfig(cfg) {
  if (!cfg.assetmaxWebClientDir || !cfg.assetmaxDir) throw new Error("Config not complete");
}

let webDirMin = path.join(cfg.assetmaxDir, "modules/app-assetmax/web/js/min")



if (cfg.keepConifgAndStuff) {
  // make folder where we store config.js and stuff
  fs.mkdirsSync(cfg.cacheDir);
  console.log(`Readding ${cfg.minFilesToKeepAfterDoGenJavascript} after do gen javascript`)
  watch(webDirMin, { recursive: true, filter: _filter }).on('change', function (evt, name) {
    let fileName = name.split('\\').slice(-1)[0];
    if (cfg.minFilesToKeepAfterDoGenJavascript.indexOf(fileName) === -1) return;
    let cacheFile = `${cfg.cacheDir}/${fileName}`
    if (evt == "remove") {
      if (fs.existsSync(cacheFile)) {
        logEvent("readding deleted file ", name)
        fs.copySync(cacheFile, name);
      }
    } else {
      // save file
      logEvent("caching file ", name)
      fs.copySync(name, cacheFile)
    }
  });
}
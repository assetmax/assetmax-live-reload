Copies files on changes in assetmax-web-client to assetmax/modules/web and caches minified files created during do-gen-javascript.

Requirements:  

- node.js (tested on 6.6.0, 8.12.0, v12.18.4)

Config (config.json):  

- set assetmaxWebClientDir to assetmax-web-client directory

- set assetmaxDir to assetmax directory

- (optional) propagateDelete, also deletes files when they are deleted in the assetmax-web-client

Run:  
- npm start
